#include "Encryption.h"


void Encryption::EncryptionInit()
{
	std::cout << "Introduce key: ";
	std::cin >> key;
	std::cout << "\n";
	std::cout << "Introduce message: ";
	std::cin >> message;
	std::cout << "\n";

	transform(message.begin(), message.end(), message.begin(), ::toupper);

	EncryptionStart();

}

void Encryption::EncryptionStart()
{

	for (int index = 0; index < message.size(); index++)
	{
		if ((message[index] + key) < 91)
		{
			message[index] = message[index] + key;
		}
		else
		{
			message[index] = (message[index] + key) - 90;
		}
	}

	EncyptionPrint();
}

void Encryption::EncyptionPrint()
{
	std::cout << "Rezultatul encriptarii este: " << message;
	std::cout << "\n";
}