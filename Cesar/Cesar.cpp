#include <iostream>
#include "Encryption.h"
#include "Decryption.h"

int main()
{
    Encryption encry;
    Decryption decry;
    int option;

    std::cout << "Hello World! What you want to do today?\n";
    std::cout << "Encrypt! Press 1\n"; 
    std::cout << "Decrypt! Press 2\n";
    std::cin >> option;

    switch (option)
    {

    case 1:
        encry.EncryptionInit();
        break;
    case 2:
        decry.DecryptionInit();
        break;
    default:
        break;
    }

}

