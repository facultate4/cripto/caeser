#pragma once
#include <iostream>
#include <string>
#include <algorithm>
class Decryption
{

public:
	void DecryptionInit();
	void DecryptionStart();
	void DecryptionPrint();
private:
	int key;
	std::string message;

};

