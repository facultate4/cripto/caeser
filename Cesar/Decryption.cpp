#include "Decryption.h"

void Decryption::DecryptionInit()
{
	std::cout << "Introduce key: ";
	std::cin >> key;
	std::cout << "\n";
	std::cout << "Introduce message: ";
	std::cin >> message;
	std::cout << "\n";

	transform(message.begin(), message.end(), message.begin(), ::toupper);
	DecryptionStart();
}

void Decryption::DecryptionStart()
{
	for (int index = 0; index < message.size(); index++)
	{
		if ((message[index] - key) > 64)
		{
			message[index] = message[index] - key;
		}
		else
		{
			message[index] = (message[index] - key) + 90;
		}
	}

	DecryptionPrint();
}

void Decryption::DecryptionPrint()
{
	std::cout << "Rezultatul encriptarii este: " << message;
	std::cout << "\n";
}
